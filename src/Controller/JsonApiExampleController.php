<?php


namespace Drupal\jsonapi_example\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Drupal\jsonapi_example\Form\SettingsForm;

const DOCUMENTATION = 'DOCUMENTATION.html';

class JsonApiExampleController extends ControllerBase {

  // Assemble some various forms and text
  public function content() {

    $documentation_content = file_get_contents(
      __DIR__
      . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR
      . DOCUMENTATION
    );
    $documentation_content = $this->t($documentation_content);

    return [
      '#attached' => [
        'library' => ['jsonapi_example/default'],
      ],
      'help' => [
        '#type' => 'details',
        '#id' => Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-help'),
        '#title' => '<h2>' . $this->t('How this work ?') . '</h2>',
        '#open' => FALSE,
        'text' => [
          '#type' => 'processed_text',
          '#format' => 'full_html',
          '#text' => $documentation_content,
        ],
        '#attributes' => [
          'class' => [
            Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-help-zone'),
          ],
        ],
      ],


      'settings' => [
        '#type' => 'details',
        '#id' => Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-settings'),
        '#open' => FALSE,
        '#title' => '<h2>' . $this->t('Settings') . '</h2>',
        '#attributes' => [
          'class' => [
            Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-settings-zone'),
          ],
        ],
        '#summary_attributes' => [
          'class' => [
            Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-summary-settings'),
          ],
        ],
        'form' => \Drupal::formBuilder()
          ->getForm('\Drupal\jsonapi_example\Form\SettingsForm'),
      ],

      'reading_zone' => [
        '#type' => 'fieldgroup',
        '#id' => Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-reading-zone'),
        '#title' => $this->t('Reading', [], ['context' => 'Fieldgroup title for reading distant article']),
        '#attributes' => [
          'class' => [
            Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-reading-zone'),
          ],
        ],
        'reading_ressources' => [
          '#type' => 'fieldgroup',
          '#id' => Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-reading-resources'),
          'form' => \Drupal::formBuilder()
            ->getForm('\Drupal\jsonapi_example\Form\ReadRessourcesForm'),
          '#description' => $this->t($this->descriptionReadingRessources()),
        ],
        'reading_articles' => [
          '#type' => 'fieldgroup',
          '#id' => Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-reading-articles'),
          'form' => \Drupal::formBuilder()
            ->getForm('\Drupal\jsonapi_example\Form\ReadArticlesForm'),
          '#description' => $this->t($this->descriptionReadingArticles()),
        ],
        'reading_article' => [
          '#type' => 'fieldgroup',
          '#id' => Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-reading-article'),
          'form' => \Drupal::formBuilder()
            ->getForm('\Drupal\jsonapi_example\Form\ReadArticleForm'),
          '#description' => $this->t($this->descriptionReadingArticle()),
        ],
      ],
      'adding_zone' => [
        '#type' => 'fieldgroup',
        '#id' => Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-adding-zone'),
        '#title' => $this->t('Adding'),
        '#attributes' => [
          'class' => [
            Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-adding-zone'),
          ],
        ],
        'adding_article' => [
          '#type' => 'fieldgroup',
          '#id' => Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-adding-article'),
          'form' => \Drupal::formBuilder()
            ->getForm('\Drupal\jsonapi_example\Form\AddArticleForm'),
          '#description' => $this->t($this->descriptionAddingArticle()),
        ],
      ],
      'editing_zone' => [
        '#type' => 'fieldgroup',
        '#id' => Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-editing-zone'),
        '#title' => $this->t('Editing'),
        '#attributes' => [
          'class' => [
            Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-editing-zone'),
          ],
        ],
        'editing_article' => [
          '#type' => 'fieldgroup',
          '#id' => Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-editing-article'),
          'form' => \Drupal::formBuilder()
            ->getForm('\Drupal\jsonapi_example\Form\EditArticleForm'),
          '#description' => $this->t($this->descriptionEditingArticle()),
        ],
      ],
      'deleting_zone' => [
        '#type' => 'fieldgroup',
        '#id' => Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-deleting-zone'),
        '#title' => $this->t('Deleting'),
        '#attributes' => [
          'class' => [
            Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-deleting-zone'),
          ],
        ],
        'delete_content' => [
          '#type' => 'fieldgroup',
          '#id' => Html::cleanCssIdentifier(SettingsForm::MODULE_NAME . '-deleting-content'),
          'form' => \Drupal::formBuilder()
            ->getForm('\Drupal\jsonapi_example\Form\DeleteArticleForm'),
          '#description' => $this->t($this->descriptiondeletingArticle()),
        ],
      ],

    ];
  }

  /**
   * @return string  Form description explaining method for listing resources
   */
  private function descriptionReadingRessources() {
    return 'GET /jsonapi';
  }

  /**
   * @return string  Form description explaining method for reading articles by
   *                 title
   */
  private function descriptionReadingArticles() {
    return 'GET /jsonapi/node/article?fields[node--article]=title';
  }

  /**
   * @return string  Form description explaining method for reading an article
   */
  private function descriptionReadingArticle() {
    return 'GET /jsonapi/node/article/&lt;UUID&gt;';
  }

  /**
   * @return string  Form description explaining method for adding an article
   */
  private function descriptionAddingArticle() {
    return '
<h2>HTTP</h2>
POST /jsonapi/node/article<br>
<h2>HEADERS</h2>
\'Authorization\': \'Bearer &lt;TOKEN&gt;\' 
<h2>BODY</h2>
<pre>{
	"data": {
		"type": "node--article",
		"attributes": {
		    "moderation_state": "published",
			"title": "Article title",
			"body": {
				"value": "Article body",
				"format": "basic_html"
			}
		}
	}
}</pre>
';
  }

  /**
   * @return string  Form description explaining method for editing an article
   */
  private function descriptionEditingArticle() {
    return '
<h2>HTTP</h2>
PATCH /jsonapi/node/article/&lt;UUID&gt;<br>
<h2>HEADERS</h2>
\'Authorization\': \'Bearer &lt;TOKEN&gt;\' 
<h2>BODY</h2>
<pre>{
	"data": {
		"type": "node--article",
		"id": "&lt;UUID&gt;",
		"attributes": {
			"title": "New title"
		}
	}
}</pre>
';
  }

  /**
   * @return string  Form description explaining method for deleting a content
   */
  private function descriptiondeletingArticle() {
    return '
<h2>HTTP</h2>
DELETE /jsonapi/node/article/&lt;UUID&gt;<br>
<h2>HEADERS</h2>
\'Authorization\': \'Bearer &lt;TOKEN&gt;\' 
';
  }
}
