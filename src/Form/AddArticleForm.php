<?php


namespace Drupal\jsonapi_example\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\jsonapi_example\RequestFactory;
use Drupal\jsonapi_example\JsonApiExampleTrait;
use Drupal\Component\Utility\Xss;
use Drupal\Component\Utility\Html;

/**
 * Form for testing adding a n article on distant JSONAPI server
 *
 * @package Drupal\jsonapi_example
 */
class AddArticleForm extends FormBase {

  use JsonApiExampleTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return SettingsForm::MODULE_NAME . '_adding_article';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Article title'),
      '#placeholder' => $this->t('Title', [], ['context' => 'Article title in action: adding an article']),
      '#size' => SettingsForm::INPUT_MAXSIZE,
      '#maxlength' => SettingsForm::INPUT_MAXLENGTH
    ];
    $form['body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body', [], ['context' => 'Title of the body field']),
      '#rows' => 2,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save article'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->getSettings();
    $titre = Html::escape($form_state->getValue('title'), 'plain_text');
    $corps = Xss::filter($form_state->getValue('body'));

    // Information message on current action
    $message = $this->t(
      "<p>Add an article</br> on source</br><strong>@source</strong></p>",
      ['@source' => $config[SettingsForm::SETTINGS_CONN]]
    );

    // Prepare and launch execution of the request
    $requete = new RequestFactory();
    $message .= $requete->addArticle($titre, $corps);

    // Display th return message or error code
    $rendered_message = \Drupal\Core\Render\Markup::create($message);
    \Drupal::messenger()->addStatus($rendered_message);
  }
}
