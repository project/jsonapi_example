<?php


namespace Drupal\jsonapi_example\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\jsonapi_example\RequestFactory;
use Drupal\jsonapi_example\JsonApiExampleTrait;


/**
 * Form to test reading titles of all articles of an distant  JSON : API server.
 *
 * @package Drupal\jsonapi_example
 */
class ReadArticlesForm extends FormBase {

  use JsonApiExampleTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return SettingsForm::MODULE_NAME . '_lecture_articles';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Read title of all articles'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->getSettings();

    // Information message relative to the current action
    $message = $this->t("<p>on source</br><strong>@source</strong></p>",
      [
        '@source' => $config[SettingsForm::SETTINGS_CONN],
      ],
      ['langcode' => 'en']
    );

    // prepare and launch execution of the request
    $requete = new RequestFactory();
    $message .= $requete->getArticles();

    // Affichage en status_message du retour de la requete
    $rendered_message = \Drupal\Core\Render\Markup::create($message);
    \Drupal::messenger()->addStatus($rendered_message);
  }
}
