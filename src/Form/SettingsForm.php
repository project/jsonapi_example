<?php


namespace Drupal\jsonapi_example\Form;

use  \Drupal\jsonapi_example\JsonApiExampleTrait;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Form for set all module settings
 *
 * @package Drupal\jsonapi_example
 */
class SettingsForm extends ConfigFormBase {

  use JsonApiExampleTrait;

  // Module machine name
  const MODULE_NAME = 'jsonapi_example';

  // Module config key
  const SETTINGS = 'jsonapi_example.settings';

  // Base URL key name
  const SETTINGS_CONN = 'chaine_connection';

  // Token getting URL key name
  const SETTINGS_TOKEN = 'url_obtention_token';

  // HTTP timeout
  const HTTP_TIMEOUT = 5;

  // Format of the exchanges
  const HEADER_ACCEPT = 'application/vnd.api+json';

  // Work with 'article' node type
  const MACHINETYPE_ARTICLE = 'node--article';

  // maximum number of column of forms
  const INPUT_MAXSIZE = 50;

  // maximum length of titles
  const INPUT_MAXLENGTH = 128;

  // Base URL example
  const PLACEHOLDER_URL_ENDPOINT = 'http://www.organisation.org:8085/jsonapi';

  // Token URL example
  const PLACEHOLDER_URL_TOKEN = 'http://www.organisation.org:8085/oauth/token';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return Self::MODULE_NAME . '_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->getSettings();

    $form['debug'] = [
      '#type' => 'radios',
      '#title' => $this->t('Debug'),
      '#default_value' => $config['debug'],
      '#options' => [
        '0' => $this->t('Normal mode'),
        '1' => $this->t('Debug'),
      ],
    ];

    $form['auto'] = [
      '#type' => 'radios',
      '#title' => $this->t('Automatic copy of new article to target JSON API server'),
      '#default_value' => $config['auto'],
      '#options' => [
        '0' => $this->t('No'),
        '1' => $this->t('Yes'),
      ],
    ];

    $form[self::SETTINGS_CONN] = [
      '#type' => 'textfield',
      '#title' => $this->t('Complete URL to JSON API service'),
      '#required' => TRUE,
      '#default_value' => $config[Self::SETTINGS_CONN],
      '#placeholder' => self::PLACEHOLDER_URL_ENDPOINT,
      '#description' => $this->t('Protocol, FQDN, TCP port and service endpoint</br>Example: @placeholder', ['@placeholder' => self::PLACEHOLDER_URL_ENDPOINT])
    ];


    $form[self::SETTINGS_TOKEN] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL to obtain the oAuth2 authentification token'),
      '#default_value' => $config[Self::SETTINGS_TOKEN],
      '#required' => TRUE,
      '#placeholder' => self::PLACEHOLDER_URL_TOKEN,
      '#description' => $this->t("Example: @placeholder", ['@placeholder' => self::PLACEHOLDER_URL_TOKEN]),
    ];

    $form['http_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#required' => TRUE,
      '#default_value' => $config['http_client_id'],
      '#placeholder' => 'Client ID',
      '#size' => 12,
    ];
    $form['http_client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client SECRET'),
      '#required' => TRUE,
      '#default_value' => $config['http_client_secret'],
      '#placeholder' => 'secret',
      '#size' => 12,
    ];

    $form['http_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Identifiant'),
      '#default_value' => $config['http_username'],
      '#required' => TRUE,
      '#placeholder' => 'germaine',
      '#size' => 12,
    ];
    $form['http_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#required' => TRUE,
      '#default_value' => $config['http_password'],
      '#placeholder' => 'Password',
      '#size' => 12,
      '#description' => $this->t('BE CAREFULL !!!! Password storage is not secure</br>Don\'t use production server credentials'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = \Drupal::service('config.factory')
      ->getEditable(self::SETTINGS);

    $config->set(
      self::SETTINGS_CONN,
      UrlHelper::stripDangerousProtocols(
        SafeMarkup::checkPlain(
          $form_state->getValue(self::SETTINGS_CONN)
        )
      )
    );
    $config->set(
      self::SETTINGS_TOKEN,
        SafeMarkup::checkPlain(
          $form_state->getValue(self::SETTINGS_TOKEN)
        )
    );
    $config->set(
      'debug',
        SafeMarkup::checkPlain(
          $form_state->getValue('debug')
        )
    );
    $config->set(
      'auto',
        SafeMarkup::checkPlain(
          $form_state->getValue('auto')
        )
    );
    $config->save();

    // Store credentials in STATE API (this is not protected, but will not be exported by the CMI)
    $credentials = [
      SettingsForm::MODULE_NAME . '_http_username' => $form_state->getValue('http_username'),
      SettingsForm::MODULE_NAME . '_http_password' => $form_state->getValue('http_password'),
      SettingsForm::MODULE_NAME . '_http_client_id' => $form_state->getValue('http_client_id'),
      SettingsForm::MODULE_NAME . '_http_client_secret' => $form_state->getValue('http_client_secret'),
    ];
    \Drupal::state()->setMultiple($credentials);
  }

  /**00,=00
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::SETTINGS,
    ];
  }
}
