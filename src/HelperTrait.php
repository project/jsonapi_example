<?php

namespace Drupal\jsonapi_example;

use \Drupal\Core\Render\Markup;

/**
 * This trait provide somme  utilities functions
 */
trait HelperTrait {

  use JsonApiExampleTrait;

  /**
   * If debug mode is set, display string parameter in warning messages
   *
   * @param string $message    message to send to translation service before displaying
   * @param string $debug      debug mode (0|1)
   * @param array  $variables  [OPTIONAL] replacement values
   * @param string $langcode   [OPTIONAL] language code
   */
  public function displayDebug(string $message, string $debug = "0", array $replacements = NULL, string $langcode = 'fr') {

    // if'debug' is on, display the message in warning mode
    if ( "1" === $debug ) {
      \Drupal::messenger()->addWarning(
        Markup::create(
          $this->t(
            $message,
            $replacements,
            ['langcode' => $langcode]
          )
        )
      );
    }

  }

  /**
   * Display message in error message box
   *
   * @param string $message    message to send to translation service, may
   *                           contain variables placeholder
   * @param array $variables   [OPTIONAL] placeholder values
   * @param string $langcode   [OPTIONAL] language code
   */
  public function displayError(string $message, array $replacements = [], string $langcode = 'fr') {
    \Drupal::messenger()->addError(
      Markup::create(
        $this->t(
          $message,
          $replacements,
          ['langcode' => $langcode]
        )
      )
    );
  }
}
