<?php

namespace Drupal\jsonapi_example;

use Drupal\Component\Serialization\Json;
use Drupal\jsonapi_example\Form\SettingsForm;
use GuzzleHttp\Exception\RequestException;

/**
 * This class provide somme important utilities tools
 */
trait JsonApiExampleTrait {

  use HelperTrait;

  /**
   * Execute an HTTP request
   * (using Guzzle library)
   *
   * @param      $url_complete  complete URL to the service (ex.:
   *                            http://www.organisation.org:8085/jsonapi)
   * @param      $operation     HTTP method (GET, DELETE, POST etc.)
   * @param null $options_http  httpClient options
   * @param null $headers       keys/values to put in HTTP headers
   * @param null $body          body content
   *
   * @return string             body of the HTTP response or error code
   * @throws \GuzzleHttp\Exception\GuzzleException  non utilisee
   */
  public function execRequest($url_complete, $operation, $options_http = NULL, $headers = NULL, $body = NULL) {
    $curent_user = \Drupal::currentUser();
    if (TRUE === $curent_user->hasPermission('using jsonapi example')) {
      $config = $this->getSettings();
      $client_http = \Drupal::httpClient();
      $initial_auth = $this->getToken($client_http);

      if (!is_null($initial_auth)) {
        $authorization = $initial_auth->token_type . ' ' . $initial_auth->access_token;

        // put token in 'Authorization' header
        $options_http['headers']['Authorization'] = $authorization;

        // warn the post content will be JSON formed
        if (in_array($operation, ['POST'])) {
          $options_http['headers']['Content-Type'] = SettingsForm::HEADER_ACCEPT;
        }
        // If the authentification is OK, process the request
        if (FALSE !== $initial_auth) {
          try {
            $retour_http = $client_http->request(
              $operation,
              $url_complete,
              $options_http
            );
            $status_code = $retour_http->getStatusCode();
            $reason_phrase = $retour_http->getReasonPhrase();
          } catch (RequestException $e) {
            $reason_phrase = $e;
          }

          // if 'success' or 'created'
          if (in_array($status_code, [200, 201]) && !is_null($retour_http)) {
            $contentJson = Json::decode($retour_http->getBody());
            // GET
            return '<details><summary>'
              . $this->t('Body of the response')
              . '</summary><pre>'
              . print_r($contentJson, TRUE)
              . '</pre></details>';
          }
          elseif

            // if 'deleted'
          (204 === $status_code) {
            return $this->t('Content deleted');
          }
          else {
            $erreur = $this->t('Returned errors: ');
            $erreur .= '<pre>' . $status_code . ' - ' . $reason_phrase . '</pre>';
            $this->displayError($erreur);
            return FALSE;
          }
        }
        else {
          \Drupal::messenger()->addError(
            $this->t('Cannot authenticate', [], ['context' => 'On the JSON API target server'])
          );
        }
      }
    }
  }

  /**
   * Load module settings
   *
   * @return array  Settings
   */
  public function getSettings() {
    $config = \Drupal::config(SettingsForm::SETTINGS);
    $settings = [
      'debug' => is_null($config->get('debug')) ? "0" : $config->get('debug'),
      'auto' => is_null($config->get('auto')) ? "0" : $config->get('auto'),
      SettingsForm::SETTINGS_CONN => is_null($config->get(SettingsForm::SETTINGS_CONN)) ? SettingsForm::PLACEHOLDER_URL_ENDPOINT : $config->get(SettingsForm::SETTINGS_CONN),
      SettingsForm::SETTINGS_TOKEN => is_null($config->get(SettingsForm::SETTINGS_TOKEN)) ? SettingsForm::PLACEHOLDER_URL_TOKEN : $config->get(SettingsForm::SETTINGS_TOKEN),
      'http_username' => \Drupal::state()
        ->get(SettingsForm::MODULE_NAME . '_http_username'),
      'http_password' => \Drupal::state()
        ->get(SettingsForm::MODULE_NAME . '_http_password'),
      'http_client_id' => \Drupal::state()
        ->get(SettingsForm::MODULE_NAME . '_http_client_id'),
      'http_client_secret' => \Drupal::state()
        ->get(SettingsForm::MODULE_NAME . '_http_client_secret'),
    ];
    $this->displayDebug(
      '<h2>DEBUG - SETTINGS:</h2><pre>@settings</pre>',
      $settings['debug'],
      ['@settings' => print_r($settings, TRUE)]
    );
    return $settings;
  }

  /**
   * oAuth2 authentification
   *
   * @param \GuzzleHttp\Client $http
   *
   * @return mixed
   */
  public function getToken(\GuzzleHttp\Client $http) {
    $config = $this->getSettings();
    if (
      is_null($config['http_client_id'])
      || is_null($config['http_client_secret'])
      || is_null($config['http_username'])
      || is_null($config['http_password'])
    ) {
      \Drupal::messenger()
        ->addError($this->t('You MUST fill-in authentification settings !'));
      return NULL;
    }

    $options = $this->getOptionsHttp();
    $options['http_errors'] = FALSE;
    $options['form_params'] = [
      'grant_type' => 'client_credentials',
      'client_id' => $config['http_client_id'],
      'client_secret' => $config['http_client_secret'],
      'username' => $config['http_username'],
      'password' => $config['http_password'],
    ];

    // request
    try {
      $request = $http->request(
        'POST',
        $config[SettingsForm::SETTINGS_TOKEN],
        $options
      );
      //      drupal_set_message($request->getStatusCode(), 'warning');
    } catch (RequestException  $e) {
      $error_handler_context = $e->getHandlerContext();
      if (is_array($error_handler_context)) {
        \Drupal::messenger()
          ->addError($this->t('Error httpClient, verify your settings: error code @error', ['@error' => $error_handler_context['errno']]));
        if($error_handler_context['errno'] === 28){
          \Drupal::messenger()
            ->addError($this->t('Can\'t connect to JSON API server, please verify server address in your settings'));
        }
        return FALSE;
      }
    }

    // return response
    if (200 == $request->getStatusCode()) {
      $response_body = json_decode($request->getBody());
      $this->displayDebug(
        '<h2>DEBUG - AUTHENTIFICATION REPONSE:</h2><pre><br />@response_body</pre>',
        $config['debug'],
        ['@response_body' => print_r($response_body, TRUE)]
      );
      return $response_body;
    }
    else {
      return FALSE;
    }
  }

  /**
   * @return array   Parameters for httpClient
   */
  public function getOptionsHttp() {
    return [
      'connect_timeout' => SettingsForm::HTTP_TIMEOUT,
      'headers' => [
        'Accept' => SettingsForm::HEADER_ACCEPT,
      ],
      'http_errors' => TRUE,
    ];
  }
}
