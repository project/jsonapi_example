<?php

namespace Drupal\jsonapi_example;

use Drupal\jsonapi_example\Form\SettingsForm;
use \Guzzle\Client;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Serialization\Json;

/**
 * This class prepare requests to JSON API server
 *
 * @package Drupal\jsonapi_example
 */
class RequestFactory {

  use StringTranslationTrait;
  use JsonApiExampleTrait;

  /**
   * Get resource list
   *
   * @return string
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getResources() {
    \Drupal::messenger()
      ->addStatus($this->t('Ressources list', [], ['context' => 'List of URL giving informations on the JSON API server.']));
    $config = $this->getSettings();

    return $this->execRequest(
      $config[SettingsForm::SETTINGS_CONN] . '/',
      'GET',
      $this->getOptionsHttp()
    );
  }

  /**
   * Request titles of all articles
   * title
   *
   * @return string
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getArticles() {
    \Drupal::messenger()
      ->addStatus($this->t('Display title of all articles'));
    $config = $this->getSettings();

    return $this->execRequest(
      $config[SettingsForm::SETTINGS_CONN] . '/node/article/?fields[' . SettingsForm::MACHINETYPE_ARTICLE . ']=title',
      'GET',
      $this->getOptionsHttp()
    );
  }

  /**
   * Request all informations of an article designed by UUID
   *
   * @param $uuid   String Universal unique  identifier
   *
   * @return string
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getArticle($uuid) {
    \Drupal::messenger()
      ->addStatus($this->t('Read an article', [], ['context' => 'Read all informations of a designated by UUID article']));
    $config = $this->getSettings();

    return $this->execRequest(
      $config[SettingsForm::SETTINGS_CONN] . '/node/article/' . $uuid,
      'GET',
      $this->getOptionsHttp()
    );
  }

  /**
   * Add an article to the target JSON API server
   *
   * @param $titre   title of the article
   * @param $corps   body of the article
   *
   * @return string
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function addArticle($titre, $corps) {
    \Drupal::messenger()
      ->addStatus($this->t('Adding an article '));
    $config = $this->getSettings();
    $options = $this->getOptionsHttp();
    $body_json = Json::encode([
      'data' => [
        'type' => SettingsForm::MACHINETYPE_ARTICLE,
        'attributes' => [
          'moderation_state' => 'published',
          'title' => $titre,
          'body' => $corps,
        ],
      ],
    ]);
    $options['body'] = $body_json;
    $options['headers']['Content-Type'] = 'application/vnd.api+json';
    return $this->execRequest(
      $config['chaine_connection'] . '/node/article/',
      'POST',
      $options
    );
  }

  /**
   * Edit an article on the target JSON API server
   *
   * @param $uuid   string Universal unique  identifier
   * @param $titre  title of the article
   *
   * @return string
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function editArticle($uuid, $titre) {
    \Drupal::messenger()
      ->addStatus($this->t('Editing an article '));
    $config = $this->getSettings();
    $options = $this->getOptionsHttp();
    $body_json = Json::encode([
      'data' => [
        "type" => "node--article",
        "id" => $uuid,
        'attributes' => [
          'title' => $titre,
        ],
      ],
    ]);
    $options['body'] = $body_json;
    $options['headers']['Content-Type'] = 'application/vnd.api+json';
    return $this->execRequest(
      $config['chaine_connection'] . '/node/article/' . $uuid,
      'PATCH',
      $options
    );
  }

  /**
   * Delete an article on the target JSON API server
   *
   * @param $uuid   string Universal unique  identifier
   *
   * @return string
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function deleteArticle($uuid) {
    \Drupal::messenger()
      ->addStatus($this->t('Deleting a content'));
    $config = $this->getSettings();
    $options = $this->getOptionsHttp();
    $options['headers']['Content-Type'] = 'application/vnd.api+json';
    return $this->execRequest(
      $config['chaine_connection'] . '/node/article/' . $uuid,
      'DELETE',
      $options
    );
  }

}
