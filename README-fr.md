CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation du serveur
 * Requis
 * Installation
 * Configuration
 * Mainteneur


INTRODUCTION
------------
Destiné à montrer comment dialoguer avec un server JSON API (Drupal 8 en l'occurence).

 * Pour une description complète de ce module, veuillez visiter la page du projet:
   [https://www.drupal.org/project/jsonapi_example](https://www.drupal.org/project/jsonapi_example)

 * Pour soummettre des rapports de bug, des suggestions fonctionnelles, ou pour suivre les changements: 
   [https://www.drupal.org/project/issues/search/jsonapi_example](https://www.drupal.org/project/issues/search/jsonapi_example)

INSTALLATION DU SERVEUR
----------------------

Sur le serveur, activer JSON API, et installer simple_oauth, 

    composer require drupal/simple_oauth
    drush en -y simple_oauth
    drush en -y jsonapi_example
    
Aller à admin/config/services/jsonapi et accepter toutes les opérations CRUD.  

Aller à admin/config/people/simple_oauth  et génerer les clés.  

Créer un rôle spécifique et définir les permissions associées. Créer le compte utilisateur appartenant à ce rôle.
  
Aller à admin/config/services/consumer et créer le client définit par ce rôle et cet utilisateur, noter le client_id et le client_secret.  

REQUIS
------

Sans objet.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.  See: [https://www.drupal.org/node/1897420](https://www.drupal.org/node/1897420)  for further information.
* Configuration et utilisation de ce module à : '/usagejsonapi'

CONFIGURATION
-------------

En haut de page, déplier la section "paramètrage".


MAINTENEUR
-----------

[Jerome LEGENDRE](https://www.drupal.org/u/jeromelegendre)
